package main

import (
	"crypto/sha256"
	"fmt"
	"io/ioutil"
	"os"
)

// if dir
//   read files and execute recSum on files
// else
//   return sha256 sum of file contents
func recursiveSum(f os.File) []byte {
}

// go run main.go filename
// if the filepath points to a directory,
// return the sum of all the sums in the directory
func main() {
	if len(os.Args) < 2 {
		fmt.Println("need a filename")
	}
	fname := os.Args[1]
	data, err := ioutil.ReadFile(fname)
	if err != nil {
		panic(err)
	}

	sum := sha256.Sum256(data)
	fmt.Printf("%x\n", sum)
}
